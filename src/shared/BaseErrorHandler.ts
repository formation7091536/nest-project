import { HttpException } from '@nestjs/common';

export class BaseErrorHanlder extends HttpException {
  constructor(message: string, statusCode: number) {
    super(message, statusCode);
  }
}

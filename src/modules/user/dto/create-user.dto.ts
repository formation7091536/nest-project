import {
  IsBase64,
  IsEmail,
  IsEmpty,
  IsEnum,
  IsNotEmpty,
  IsString,
  MinLength,
} from 'class-validator';
import { User } from '../user.entity';

export class CreateUserDto extends User {
  @IsEmpty()
  id: number;

  @IsString()
  @MinLength(3)
  firstName: string;

  @IsString()
  @MinLength(3)
  lastName: string;

  @IsString()
  @IsEmail({}, { message: 'Invalid email' })
  email: string;

  @IsString()
  @MinLength(8)
  @IsBase64()
  password: string;

  @IsEnum({ admin: 'admin', teacher: 'teacher', student: 'student' })
  @IsNotEmpty()
  role: string;

  @IsEmpty()
  isActive: boolean;
}
